// ==UserScript==
// @name          Koreus v2
// @namespace     http://userstyles.org
// @description	  Patch de personnalisation du site koreus.com
// @author        AlTi5
// @include       http://www.koreus.com/*
// @include       https://www.koreus.com/*
// @include       http://*.www.koreus.com/*
// @include       https://*.www.koreus.com/*
// @run-at        document-start
// @version       0.20160626203137
// ==/UserScript==
(function () {
    var css = [
        "tr .head td  { 	 padding:10px;     border-top: #fff solid 1px!important;} ",
        " tr .head td:first-child {border-radius:10px 0 0 10px}",
        " tr .head td:last-child {border-radius:0px 10px 10px 0}",
        " .outer .dropdown {width:50%}",
        " .outer .dropdown .menu{width:100%}",
        "td#leftcolumn div.blockTitle {    color: #306;    background: linear-gradient(#ccc, #fff)!important;    padding: 4px 3px 4px 3px;}    ",
        ".comText iframe {height: 902px;    width: 500px;}"
    ].join("\n");
    if (typeof GM_addStyle != "undefined") {
        GM_addStyle(css);
    } else if (typeof PRO_addStyle != "undefined") {
        PRO_addStyle(css);
    } else if (typeof addStyle != "undefined") {
        addStyle(css);
    } else {
        var node = document.createElement("style");
        node.type = "text/css";
        node.appendChild(document.createTextNode(css));
        var heads = document.getElementsByTagName("head");
        if (heads.length > 0) {
            heads[0].appendChild(node);
        } else {
            // no head yet, stick it whereever
            document.documentElement.appendChild(node);
        }
    }
})();
